package s1ex2;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Vehicle implements Runnable {
    String brand;
    int year;
    int bound;
    Random r = new Random();
    List<Vehicle> vehicleList;

    public Vehicle(String brand, int year) {
        this.brand = brand;
        this.year = year;
    }

    public Vehicle(int bound, List<Vehicle> vehicleList) {
        this.bound = bound;
        this.vehicleList = vehicleList;
    }

    public void run() {
        try {
            while(1<2) {
                Vehicle v = new Vehicle("vw", 2015);
                vehicleList.add(v);
                int rand = r.nextInt(bound);
                System.out.println("Vehicle added: " + v + " time to wait: " + rand);
                Thread.sleep(rand);
            }
        } catch (Exception e) {
        }
    }

    @Override
    public String toString() {
        return brand + " " + year;
    }

    public static void main(String[] args) {
        List<Vehicle> vehicleList = new ArrayList<>();
        Thread t1 = new Thread(new Vehicle(2000, vehicleList));
        Thread t2 = new Thread(new Monitor(vehicleList));
        t1.start();
        t2.start();
        new GUI(vehicleList);
    }
}

class Monitor implements Runnable {
    List<Vehicle> vehicleList;

    public Monitor(List<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }

    public void run() {
        try {
            int size, prevsize = 0;
            while (1 < 2) {
                size = vehicleList.size();
                if (prevsize < size) {
                    System.out.println(size);
                }
                while (size >= 10) {
                    vehicleList.remove(0);
                    System.out.println("Vehicle removed");
                    size = vehicleList.size();
                }
                prevsize = size;
                Thread.sleep(1000);
            }
        } catch (Exception e) {
        }
    }
}

class GUI {
    JTextField textField1 = new JTextField();
    JTextField textField2 = new JTextField();

    public GUI(List<Vehicle> vehicleList) {
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(50, 50, 50, 50));
        //panel.setLayout(new GridLayout(0, 1));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(textField1);
        panel.add(textField2);

        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Subiect1Ex3");
        //frame.pack();
        frame.setSize(new Dimension(700,200));
        frame.setVisible(true);
        while (1 < 2) {
            textField1.setText("The size of the List: " + vehicleList.size());
            textField2.setText(String.valueOf(vehicleList));
        }
    }
}
