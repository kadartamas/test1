package s3ex1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

public class GUI implements ActionListener {
    JTextField textField1 = new JTextField();
    JTextField textField2 = new JTextField();
    JButton button = new JButton();
    JLabel label1 = new JLabel();
    JLabel label2 = new JLabel();

    public GUI() throws IOException {
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(50,50,50,50));
        panel.setLayout(new GridLayout(0,1));
        label1.setPreferredSize(new Dimension(100,20));
        label1.setText("Text de salvat");
        panel.add(label1);
        textField1.setPreferredSize(new Dimension(100,20));
        panel.add(textField1);
        //label2.setPreferredSize(new Dimension(100,20));
        label2.setText("Nume fisier");
        panel.add(label2);
        //textField2.setPreferredSize(new Dimension(100,20));
        panel.add(textField2);
        button.addActionListener(this);
        button.setText("Salveaza");
        panel.add(button);

        frame.add(panel,BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Examen");
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) throws IOException {
        new GUI();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent){
        String text_to_write     =   textField1.getText();
        String location  =   textField2.getText();
        try {
            FileWriter fileWriter = new FileWriter(location);
            fileWriter.write(text_to_write);
            fileWriter.close();
        }
        catch (IOException e)
        {
            System.out.println("Error");
        }
    }
}
